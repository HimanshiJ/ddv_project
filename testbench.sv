`timescale 1ns / 1ps

module test_1;

	// Inputs
	reg [31:0] a_original;
	reg [31:0] b_original;
	reg start;
	reg clk;
	reg rst;

	// Outputs
	wire [31:0] mult_output;
	wire done_stage4_reg;

	// Instantiate the Unit Under Test (UUT)
	fp_mult_seq uut (
		.mult_output(mult_output), 
		.done_stage4_reg(done_stage4_reg), 
		.a_original(a_original), 
		.b_original(b_original), 
		.start(start), 
		.clk(clk), 
		.rst(rst)
	);

initial begin
		$dumpfile("mydump.vcd");
  	  	$dumpvars;
		$display("\t\ttime,\tclk,\tinA,\tinB,\tStart,\toutMult,\tready"); 
		$monitor("%d,\t%b,\t%b,\t%b,\t%d,\t%b,\t%b",$time, clk,a_original,b_original,start,mult_output,done_stage4_reg); 
		start = 0;
		clk = 0;
		#50;
                // Initialize Inputs
                rst=1;
                
                #50;
                rst=0;
                #50;
		// Initialize Inputs
	
		//normal input set1
		start=1;
		a_original = 32'b01000001101110110000000000000000; //23.375
		b_original = 32'b11010000011010110000000000000000; //-15770583040
		#40;
		start=0;
		
		#2000;

		//normal input set2
		start=1;
		a_original = 32'b01100001101110110000010000010010; 
		b_original = 32'b11010010011010110000000100100001; 
		#40;
		start=0;


		// input set 3
		
		#2000;
		start=1;
		a_original = 32'b01001001101110110001000000000000; 
		b_original = 32'b11010000011010110000000010000000; 
		#40;
		start=0;
		
		#2000;
		//input set 4 - special values
		start=1;
		a_original = 32'b01000001101110110000000000000000; 
		b_original = 32'b11010000011010110000000000000000;
		#40;
		start=0;


		#50;
		//normal input set 5
		start=1;
		a_original = 32'b01010001101110110000000000001000; //23.375
		b_original = 32'b11010010011010110000000000000001; //-15770583040
		#40;
		start=0;

		#2000;
		//input set 6 -denormal value
		start=1;
		a_original = 32'b01100001101110110000010000010010; 
		b_original = 32'b10000000011010110000000100100001; 
		#40;
		start=0;

		#1500;
		start=1;
		//input set 7 - only one input (should behave like a normal multiplication)
		b_original = 32'b11010000011010110000000000000000;
		#40;
		start=0;

		#1500;
		$finish;
		// Add stimulus here

	end
	
	always begin
	
	#10 clk=~clk;
	//start=1;
	
	end
      
endmodule