
`timescale 1ns / 1ps

module fp_mult_seq(mult_output,done_stage4_reg,a_original,b_original,start,clk, rst);

   input [31:0] a_original, b_original;
   input        start, clk, rst;
   output [31:0] mult_output;
   output        done_stage4_reg;
   reg           multneg;
   wire [7:0]    multexp;
   wire [51:0]   multsig;
	wire [22:0]   mantissa;
	wire sgn;
   assign        mult_output[31]    = sgn;
   assign        mult_output[30:23] = multexp;
   assign        mult_output[22:0]  = mantissa;

   wire [31:0]   a, b,adff,bdff;
   wire [25:0]   asig, bsig ,asigdff,bsigdff;
   wire [7:0]    aexp, bexp,aexpdff,bexpdff;
   wire          aneg, bneg,anegdff,bnegdff;
   wire  [7:0]   exp,expdff;
	wire busy_stage2;
	wire done_stage1;
	wire ip_frm_mult;
	wire flag_stage4;
	wire busy_stage3_reg,done_stage3_reg,mult_start_reg;
	wire [51:0] multsig_wr,multsig_wrdff;
	//initial flag_stage4=0;
	wire [25:0] to_mult_a, to_mult_b, to_mult_b_dff, to_mult_a_dff;
	comb_1 C1(start,a,b,clk, rst,a_original,b_original,busy_stage2,flag_stage4,done_stage1);
	comb_2 C2(aneg,bneg,aexp,bexp,exp,sgn,asig,bsig,clk, rst,a,b,done_stage1,flag_stage4,busy_stage2,done_stage2_reg);
	dff_sync_reset DFF3(aneg,clk,rst,anegdff);
	dff_sync_reset DFF4(bneg,clk,rst,bnegdff);
	dff_sync_reset8 DFF5(aexp,clk,rst,aexpdff);
	dff_sync_reset8 DFF6(bexp,clk,rst,bexpdff);
	dff_sync_reset8 DFF7(exp,clk,rst,expdff);
	dff_sync_reset DFF8(sgn,clk,rst,sgndff);
	dff_sync_reset26 DFF9(asig,clk,rst,asigdff);
	dff_sync_reset26 DFF10(bsig,clk,rst,bsigdff);
	comb_3 C3(start,ip_frm_mult,clk, rst,done_stage2_reg,flag_stage4,busy_stage3_reg,done_stage3_reg,mult_start_reg,asigdff,bsigdff,to_mult_a,to_mult_b);
	dff_sync_reset26 DFF11(to_mult_a,clk,rst,to_mult_a_dff);
    dff_sync_reset26 DFF12(to_mult_b,clk,rst,to_mult_b_dff);
	comb_4 C4(flag_stage4,mantissa,multexp,clk, rst,multsig_wrdff,expdff,busy_stage4_reg,done_stage4_reg,ip_frm_mult,done_stage3_reg);
	seq_mult M1(rst,multsig_wr, ip_frm_mult, mult_start_reg,clk, to_mult_a_dff, to_mult_b_dff);
	dff_sync_reset52 DFF13(multsig_wr,clk,rst,multsig_wrdff);
		
endmodule

module comb_1(start,a,b,clk, rst,a_original,b_original,busy_stage2,flag_reg,done_reg);

input start;
input clk,rst,busy_stage2;
input [31:0] a_original, b_original;
output reg [31:0] a,b;
output flag_reg,done_reg;
//reg flag_reg;
//assign flag_reg=0;

reg flag,done;
assign flag_reg=flag;
assign done_reg=done;
parameter state_inital_stage1=3'd0 ,state_spical_condition_stage1=3'd1,state_closed_stage1=3'd2,state_finlize_stage1=3'd3;
reg[1:0] state;
reg[2:0] next_state;
//reg flag_reg;
//initial flag_reg=0;
  
  
  //----------------------------------------------------------------------------------------------
// Assertion 1a : check if busy_stage2=1(active low) and start=1(active high) activates stage 1 by changing state to state_spical_condition_stage1
property verify_stage1_start;
@(posedge clk) 
  (!busy_stage2 && start && (next_state==state_inital_stage1)) |-> ##1 next_state == state_spical_condition_stage1;
endproperty 
//----------------------------------------------------------------------------------------------


//----------------------------------------------------------------------------------------------
// Assertion 1b : check if changing state to state_spical_condition_stage1 has done_reg and flag_reg set to 0
property verify_stage1_output;
@(posedge clk) 
	(state == state_spical_condition_stage1) |-> ((done_reg==0)&&(flag_reg==0)) ;
endproperty 
//----------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------
// Assertion 2a : check if a special value changes state to state_closed_stage1
property verify_stage1_special_values;
@(posedge clk) 
	(state == state_spical_condition_stage1) |-> 
		if(a_original == 32'b0 || b_original == 32'b0)
				##1 (state==state_closed_stage1)
		else if(a_original == 32'b10000000000000000000000000000000 || b_original == 32'b10000000000000000000000000000000)
				##1 (state==state_closed_stage1)
		else if(a_original == 32'b01111111111100000000000000000000 || b_original == 32'b01111111111100000000000000000000)
				##1 (state==state_closed_stage1)
		else if(a_original == 32'b01111111100000000000000000000000 || b_original == 32'b01111111100000000000000000000000)
				##1 (state==state_closed_stage1)
		else if(a_original == 32'b11111111100000000000000000000000 || b_original == 32'b11111111100000000000000000000000)
				##1 (state==state_closed_stage1)
		else if((a_original[30:23] == 8'b0 && a_original[22:0] != 23'b0) || (b_original[30:23] == 8'b0 && b_original[22:0] != 23'b0))
				##1 (state==state_closed_stage1)
		else 
				##1 (state==state_finlize_stage1);
endproperty 
//----------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------
// Assertion 2b : check if changing state to state_closed_stage1 has done_reg and flag_reg set to 1
property verify_stage1_closed_output;
@(posedge clk) 
	(state == state_closed_stage1) |-> ((done_reg==1)&&(flag_reg==1)) ;
endproperty 
//----------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------
// Assertion 2c : check if changing state to state_finalized_stage1 has done_reg =1 and flag_reg = 0
property verify_stage1_finalize_output;
@(posedge clk) 
	(state == state_finlize_stage1) |-> ((done_reg==1)&&(flag_reg==0)) ;
endproperty 
//----------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------
// Assertion 3a : check if state_finalized_stage1 changes back to state_initial_stage1
property verify_stage1_finalize_to_initial;
@(posedge clk) 
	(state == state_finlize_stage1) |-> ##1 (state == state_inital_stage1) ;
endproperty 
//----------------------------------------------------------------------------------------------


//----------------------------------------------------------------------------------------------
// Assertion 3b : check if state_finalized_stage1 to state_initial_stage1 changes done_reg to 0
property verify_stage1_initial_output;
@(posedge clk) 
	(state == state_finlize_stage1) |-> ##1 (done_reg==0)&&(flag_reg==0) ;
endproperty 
//----------------------------------------------------------------------------------------------


//----------------------------------Asserting the assertions----------------------------------------------------------------
assert property(verify_stage1_start) else $error("Assertion 1a has failed");
assert property(verify_stage1_output) else $error("Assertion 1b has failed");
assert property(verify_stage1_special_values) else $error("Assertion 2a has failed");
assert property(verify_stage1_closed_output) else $error("Assertion 2b has failed");
assert property(verify_stage1_finalize_output) else $error("Assertion 2c has failed");
assert property(verify_stage1_finalize_to_initial) else $error("Assertion 3a has failed");
assert property(verify_stage1_initial_output) else $error("Assertion 3b has failed");
  
  
always @(posedge clk)
begin
	if(rst) begin
		state<=state_inital_stage1;
		flag=0;
		done=0;
		a=0;
		b=0;
		//a_original<=0;
		//b_original=0;
		end
	else
		state<=next_state;
end
always @(state)
begin
	case(state)
state_inital_stage1: 
	begin
		flag=0;
		done<=0;
			a=a_original;
			b=b_original;
			
		
	end
state_spical_condition_stage1: 
	begin
		flag=0;
		done<=0;
	end
state_closed_stage1: 
	begin
		flag=1;
		done<=1;
	end
state_finlize_stage1:
	begin
	flag=0;
	done<=1;
	end
endcase
end
////////////////////////state transition
always @(state or posedge clk)
begin
	case(state)
state_inital_stage1:
begin
	if(busy_stage2==0&&start==1)
	next_state<=state_spical_condition_stage1;
else begin
	next_state<=state_inital_stage1;

end
end
state_spical_condition_stage1:
begin
			a=a_original;
			b=b_original;
		if(a_original == 32'b0 || b_original == 32'b0)
				next_state<=state_closed_stage1;
			
			else if(a_original == 32'b10000000000000000000000000000000 || b_original == 32'b10000000000000000000000000000000)
				next_state<=state_closed_stage1;
			
			else if(a_original == 32'b01111111111100000000000000000000 || b_original == 32'b01111111111100000000000000000000)
				next_state<=state_closed_stage1;
			
			else if(a_original == 32'b01111111100000000000000000000000 || b_original == 32'b01111111100000000000000000000000)
				next_state<=state_closed_stage1;
			
			else if(a_original == 32'b11111111100000000000000000000000 || b_original == 32'b11111111100000000000000000000000)
				next_state<=state_closed_stage1;
				
			else if((a_original[30:23] == 8'b0 && a_original[22:0] != 23'b0) || (b_original[30:23] == 8'b0 && b_original[22:0] != 23'b0))
                next_state<=state_closed_stage1;
			
			
			else 
			next_state<=state_finlize_stage1;
			end
			state_closed_stage1:begin
			next_state<=state_inital_stage1;
			
			end
	state_finlize_stage1:
	begin
		next_state<=state_inital_stage1;
	end
default:	next_state<=state_inital_stage1;


endcase
end
endmodule


module comb_2(aneg,bneg,aexp,bexp,exp,sgn,asig,bsig,clk, rst,a,b,done_stage1,flag_stage4,busy_stage2_reg,done_stage2_reg);

input clk,rst,done_stage1,flag_stage4;
//wire flag_stage4;
input [31:0] a, b;
output reg aneg,bneg;
output reg [7:0] aexp, bexp;
output reg [7:0] exp;
output reg sgn;
output reg [25:0] asig,bsig;
output busy_stage2_reg,done_stage2_reg;
reg busy_stage2,done_stage2;
assign busy_stage2_reg=busy_stage2;
assign done_stage2_reg=done_stage2;
parameter state_inital=2'd0 ,state_extraction=2'd1,state_closed=2'd2,state_dump=2'd3;
reg[2:0] state;
reg[2:0] next_state;
  

//----------------------------------------------------------------------------------------------
// Assertion 4a : check if state = state_intial, busy_stage2 is set to 1 and done_stage2 is set to 0
property verify_stage2_state_inital;
@(posedge clk) 
  (state==state_inital) |-> ((busy_stage2==0)&&(done_stage2==1));
endproperty 
//----------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------
// Assertion 4b : check if state = state_intial, and flag_stage4 = 0 and done_stage1 = 1 (and busy_stage3 is not active) then state should change to state_extraction
property verify_stage2_state_intial_to_extraction;
@(posedge clk) 
  (state==state_inital)&&(flag_stage4==0)&&(done_stage1==1) |-> ##1 next_state==state_extraction;
endproperty 
//----------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------
// Assertion 4c : check if state = state_intial, and flag_stage4 = 0 then state should change to state_dump
property verify_stage2_state_intial_to_dump;
@(posedge clk) 
	(state==state_inital)&&(flag_stage4==1) |-> ##1 state==state_dump;
endproperty 
//----------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------
// Assertion 5a : check if state = state_extraction, then all necessary assignments are happening or not
property verify_stage2_state_extraction;
@(posedge clk) 
	(state==state_extraction) |-> ##[1:5] //not sure how much time will it take
		(aneg == a[31] &&     
		bneg == b[31] &&
	        aexp == a[30:23] &&  
		bexp == b[30:23] &&
		asig == { 2'b0, 1'b1, a[22:0] } &&
	        bsig == { 2'b0, 1'b1, b[22:0] } &&            
        	(exp == (aexp + bexp - 127)) &&
		sgn==aneg^bneg);
endproperty 
//----------------------------------------------------------------------------------------------


//----------------------------------------------------------------------------------------------
// Assertion 5b : check if state = state_extraction, then state should change to state_closed
property verify_stage2_state_extraction_to_close;
@(posedge clk) 
	(state==state_extraction) |-> ##1 state==state_closed;
endproperty 
//----------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------
// Assertion 6a : check if state = state_closed, then busy_stage2=0 and done_stage2=1
property verify_stage2_state_close;
@(posedge clk) 
	(state==state_closed) |-> busy_stage2==0 && done_stage2==1 ;
endproperty 
//----------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------
// Assertion 6b : check if state = state_closed, then state should change to state_inital
property verify_stage2_state_closed_to_initial;
@(posedge clk) 
	(state==state_closed) |-> ##1 state==state_inital ;
endproperty 
//----------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------
// Assertion 7a : check if state = state_dump, then these condition shuold follows
property verify_stage2_state_dump;
@(posedge clk) 
	(state==state_dump) |->  
		aneg == 0 &&
		bneg == 0 &&
	        aexp == 0 &&
		bexp == 0 &&
		asig == 0 &&
        	bsig == 0 &&
	        exp == 0 &&
		sgn == 0;
endproperty 
//----------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------
// Assertion 7b : check if state = state_dump, then state should change to state_closed
property verify_stage2_state_dump_to_closed;
@(posedge clk) 
	(state==state_dump) |-> ##1 state==state_closed ;
endproperty 
//----------------------------------------------------------------------------------------------

//----------------------------------Asserting the assertions----------------------------------------------------------------
assert property(verify_stage2_state_inital) else $error("Assertion 4a has failed");
assert property(verify_stage2_state_intial_to_extraction) else $error("Assertion 4b has failed");
assert property(verify_stage2_state_intial_to_dump) else $error("Assertion 4c has failed");
assert property(verify_stage2_state_extraction) else $error("Assertion 5a has failed");
assert property(verify_stage2_state_extraction_to_close) else $error("Assertion 5b has failed");
assert property(verify_stage2_state_close) else $error("Assertion 6a has failed");
assert property(verify_stage2_state_closed_to_initial) else $error("Assertion 6b has failed");
assert property(verify_stage2_state_dump) else $error("Assertion 7a has failed");
assert property(verify_stage2_state_dump_to_closed) else $error("Assertion 7b has failed");
//--------------------------------------------------------------------------------------------------------------------------



always @(posedge clk)
begin
	if(rst) begin
		state<=state_inital;
		busy_stage2=0;
		done_stage2=1;
		aneg=0;
		bneg=0;
		exp=0;
		sgn=0;
		asig=0;
		bsig=0;
		end
	else
		state<=next_state;
end

always @(state)
begin
	case(state)
state_inital: 
	begin
		busy_stage2=0;
	    done_stage2=1;
	end
state_extraction: begin
            busy_stage2=1;
            done_stage2=0;
			aneg = a[31];
			bneg = b[31]; //holds sign bit
            aexp = a[30:23];
            bexp = b[30:23];
				
			asig = { 2'b0, 1'b1, a[22:0] };
            bsig = { 2'b0, 1'b1, b[22:0] };
            
            exp = aexp + bexp - 127;
			sgn=aneg^bneg;
			end
state_closed: 
	begin
		busy_stage2=0;
		done_stage2=1;
	end
state_dump:
	begin
	   aneg = 0;
	   bneg = 0; //holds sign bit
       aexp = 0;
       bexp = 0;		
	   asig = 0;
       bsig = 0;          
       exp = 0;
	   sgn=0;
				//dumpped=1;
	end
endcase
end
////////////////////////state transition
always@(posedge flag_stage4) begin
next_state<=state_dump;
end

always @(state or posedge clk)
begin
	case(state)
state_inital:
begin
	if(!flag_stage4) 
		begin
			if(done_stage1==1)
			     next_state<=state_extraction;
			else begin
			     next_state<=state_inital;
			end
		end
	else
		next_state<=state_dump;

end

state_extraction:
begin
    next_state<=state_closed;		
end

state_closed:
begin
    next_state<=state_inital;
end

state_dump:
begin
    next_state<=state_closed;
end

default:	next_state<=state_inital;


endcase
end
endmodule


module comb_3(start,ip_frm_mult,clk, rst,done_stage2,flag_stage4,busy_stage3_reg,done_stage3_reg,mult_start_reg,asig,bsig,to_mult_a,to_mult_b);

input start,clk,rst,ip_frm_mult,flag_stage4,done_stage2;
input [25:0] asig,bsig;
//reg abrakadbra;
output [25:0] to_mult_a;
output [25:0] to_mult_b;
output busy_stage3_reg,done_stage3_reg,mult_start_reg;
reg busy_stage3,done_stage3,mult_start;
reg [25:0] to_mult_a_reg, to_mult_b_reg;
reg [25:0] prev_a,prev_b;
assign to_mult_a=to_mult_a_reg;
assign to_mult_b=to_mult_b_reg;
assign busy_stage3_reg=busy_stage3;
assign mult_start_reg=mult_start;
assign done_stage3_reg=done_stage3;
parameter state_inital=2'd0 ,state_start_multiply=2'd1,state_closed=2'd2,state_process=2'd3;

reg[2:0] state;
reg[2:0] next_state;
  
//--------------------------------------------------Assertions------------------------------------------------------------

//----------------------------------------------------------------------------------------------
// Assertion 8a : check if state = state_intial and done_stage2 is set to 1 then state should be state_multiply
property verify_stage3_state_inital_to_multiply;
@(posedge clk) 
  ((state==state_inital)&&(done_stage2==0) && (prev_a!=asig && prev_b!=bsig) )|=>state==state_start_multiply;
endproperty 
//----------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------
// Assertion 8b : check if state = state_intial then following conditions should be true...
property verify_stage3_state_intial;
@(posedge clk) 
	(state==state_inital) |-> busy_stage3==1 &&
				  done_stage3==0 &&
				  mult_start==0;
			
endproperty 
//----------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------
// Assertion 9a : check if state = state_start_multiply then mult_start==1
property verify_stage3_state_start_multiply;
@(posedge clk) 
	(state==state_start_multiply) |-> mult_start==1;
endproperty 
//----------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------
// Assertion 9b : check if state = state_start_multiply then state will change to state_process in next cycle....
property verify_stage3_state_start_multiply_to_process;
@(posedge clk) 
	(state==state_start_multiply) |-> ##1 state==state_process;
endproperty 
//----------------------------------------------------------------------------------------------


//----------------------------------------------------------------------------------------------
// Assertion 10a : check if state = state_process and && ip_frm_mult==1, then state should change to state_closed
property verify_stage3_state_process_to_closed;
@(posedge clk) 
  (next_state==state_process && ip_frm_mult==1) |=>  next_state==state_closed;


endproperty 
//----------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------
// Assertion 11a : check if state = state_closed, then these conditions should be true..
property verify_stage3_state_closed;
@(posedge clk) 
	(state==state_closed) |-> busy_stage3==0 &&
				  done_stage3==1 &&
		  		  mult_start==0; 
endproperty 
//----------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------
// Assertion 11b : check if state = state_closed, then state should change to state_inital
property verify_stage3_state_closed_to_initial;
@(posedge clk) 
	(state==state_closed) |-> ##1 state==state_inital ;
endproperty 
//----------------------------------------------------------------------------------------------



//----------------------------------Asserting the assertions----------------------------------------------------------------
assert property(verify_stage3_state_inital_to_multiply) else $error("Assertion 8a has failed");
//assert property(verify_stage3_state_intial) else $error("Assertion 8b has failed");
assert property(verify_stage3_state_start_multiply) else $error("Assertion 9a has failed");
assert property(verify_stage3_state_start_multiply_to_process) else $error("Assertion 9b has failed");
assert property(verify_stage3_state_process_to_closed) else $error("Assertion 10a has failed");
assert property(verify_stage3_state_closed) else $error("Assertion 11a has failed");
assert property(verify_stage3_state_closed_to_initial) else $error("Assertion 11b has failed");

//--------------------------------------------------------------------------------------------------------------------------


always @(posedge clk)
begin
	if(rst) begin
		state=0;
		next_state=0;
		busy_stage3=0;
		mult_start=0;
		done_stage3=0;
		to_mult_a_reg=0;
		to_mult_b_reg=0;
		prev_a=0;
		prev_b=0;
		end
	else
		state<=next_state;
end

always @(flag_stage4) begin
busy_stage3=0;
		done_stage3=1;
		mult_start=0;
		next_state=state_closed;
end

always @(state)
begin
	case(state)
	
state_inital: 
	begin
		busy_stage3=0;
		done_stage3=0;
		mult_start=0;	
	end
	
state_start_multiply: 
begin
    to_mult_a_reg=asig;
    to_mult_b_reg=bsig;
    prev_a=asig;
    prev_b=bsig;
    mult_start=1;
end

state_closed: 
	begin
		busy_stage3=0;
		done_stage3=1;
		mult_start=0;
		
	end
state_process:
    begin
        busy_stage3=1;
    end
endcase
end

always @(state or posedge clk)
begin
	case(state)
state_inital:
begin
	if(done_stage2==0 || (prev_a!=asig && prev_b!=bsig) )
	next_state<=state_start_multiply;
else begin
	next_state<=state_inital;

end
end

state_start_multiply:
begin
next_state<=state_process;		
end

state_process: begin
mult_start=0;
if(ip_frm_mult==1)
next_state<=state_closed;
else
next_state<=state_process;
end

state_closed:
begin
next_state<=state_inital;
end
default:	next_state<=state_inital;


endcase
end

endmodule

module comb_4(flag_stage4,mantissa,multexp,clk, rst,multsig,diff,busy_stage4_reg,done_stage4_reg,done_mult,done_stage3_reg);

input done_mult,flag_stage4;
input clk,rst,done_stage3_reg;
input [51:0] multsig;
input [7:0] diff;
output [22:0] mantissa;
output [7:0] multexp;
reg [22:0] mantissa_reg;
reg [7:0] multexp_reg;

assign mantissa=mantissa_reg;
assign multexp=multexp_reg;

output busy_stage4_reg,done_stage4_reg;
reg busy_stage4,done_stage4;
assign busy_stage4_reg=busy_stage4;
assign done_stage4_reg=done_stage4;
parameter state_inital=2'd0 ,state_finiliazing=2'd1,state_closed=2'd2,state_dummy=2'd3;

reg[2:0] state;
reg[2:0] next_state;
  
//--------------------------------------------------Assertions------------------------------------------------------------

//----------------------------------------------------------------------------------------------
// Assertion 12a : check if state = state_intial then state will change to state_finiliazing in next cycle...
property verify_stage4_state_inital_to_finiliazing;
@(posedge clk) 
  ((next_state==state_inital)&& (done_mult==1))|-> ##1 next_state==state_finiliazing;
endproperty 
//----------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------
// Assertion 13a : check if state = state_finiliazing then following conditions should be true...
property verify_stage4_state_finiliazing;
@(posedge clk) 
	(state==state_finiliazing)  |-> if( multsig[47] ) ##1 ((multexp == diff + 1) && ( mantissa == multsig[46:24] ))
				 	else ##1 ((multexp == diff) && (mantissa == multsig[47:25]));
endproperty 
//----------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------
// Assertion 13b : check if state = state_finiliazing then state will change to state_process in next cycle....
property verify_stage4_state_finiliazing_to_state_closed;
@(posedge clk) 
	((state==state_finiliazing) && (done_stage3_reg==1))|-> ##1 state==state_closed;
endproperty 
//----------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------
// Assertion 14a : check if state = state_closed, then these conditions should be true..
property verify_stage4_state_closed;
@(posedge clk) 
	(state==state_closed) |-> busy_stage4==0 &&
				  done_stage4==1; 
endproperty 
//----------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------
// Assertion 14b : check if state = state_closed, then state should change to state_inital
property verify_stage4_state_closed_to_initial;
@(posedge clk) 
	(state==state_closed) |-> ##1 state==state_inital ;
endproperty 
//----------------------------------------------------------------------------------------------


//----------------------------------------------------------------------------------------------
// Assertion 15a : check if state = state_dummy then these conditions should be true..
//property verify_stage4_state_dummy;
//@(posedge clk) 
//	(state==state_dummy) |->  multexp==0 &&
 //				    mantissa==0;


//endproperty 
//----------------------------------------------------------------------------------------------

//----------------------------------Asserting the assertions----------------------------------------------------------------
assert property(verify_stage4_state_inital_to_finiliazing) else $error("Assertion 12a has failed");
//assert property(verify_stage4_state_intial) else $error("Assertion 12b has failed");
assert property(verify_stage4_state_finiliazing) else $error("Assertion 13a has failed");
assert property(verify_stage4_state_finiliazing_to_state_closed) else $error("Assertion 13b has failed");
assert property(verify_stage4_state_closed) else $error("Assertion 14a has failed");
assert property(verify_stage4_state_closed_to_initial) else $error("Assertion 14b has failed");
//assert property(verify_stage4_state_dummy) else $error("Assertion 15a has failed");
//--------------------------------------------------------------------------------------------------------------------------


always @(posedge clk)
begin
	if(rst) begin
		state<=state_inital;
		mantissa_reg=0;
		multexp_reg=0;
		busy_stage4=0;
		done_stage4=0;
		end
	else
		state<=next_state;
end

always @(state)
begin
	case(state)
state_inital:

	begin
		busy_stage4=1;
		done_stage4=0;
	end
state_finiliazing: 

begin
if ( multsig[47] ) begin:A
                  multexp_reg <= diff + 1;
                  mantissa_reg <= multsig[46:24];
            end 
				
				else begin:B
                  multexp_reg <= diff;
                  mantissa_reg <= multsig[47:25];

				end
		//done_stage4=1;
 end
 
 state_dummy:
 begin
 multexp_reg=0;
 mantissa_reg=0;
 end
state_closed: 
	begin
		busy_stage4=0;
		done_stage4=1;
	end
endcase
end

always @(state or posedge clk)
begin
	case(state)
state_inital:
begin
	if(done_mult==1)
	next_state<=state_finiliazing;
else begin
	next_state<=state_inital;

end
end
state_finiliazing:

begin
if(done_stage3_reg==1)
next_state<=state_closed;
else
	next_state<=state_finiliazing;	
end
state_closed:
begin
next_state<=state_inital;
end
default:	next_state<=state_inital;


endcase
end

endmodule


`define width 26
`define ctrwidth 4
module seq_mult (rst,
		 // Outputs
		 p, mult_ready, start, 
		 // Inputs
		 clk, a, b
		 ) ;
   input clk,rst;
	input start;
   input [`width-1:0] a, b;
   output [2*`width-1:0] p;
   // *** Output declaration for 'p'
   output mult_ready;
   reg [2*`width-1:0] p,multiplicand,multiplier;
	reg [`width-1:0] prev_a,prev_b;
   // *** Register declarations for p, multiplier, multiplicand
   reg mult_ready_1=0;
	assign mult_ready=mult_ready_1;
	
   reg [`ctrwidth:0] ctr;
	//reg reset;
	//initial reset=1;
	reg [2:0]     state;
	//reg [2:0]     next_state;
	
	parameter     st_idle  = 0;
	parameter     setup  = 3;
   parameter     st_cyc_1 = 1;
	reg enable;

  
//----------------------------------------------------------------------------------------------
// Assertion 16a : check if start is set
property verify_stage_seq_mult;
@(posedge clk) 
	start |-> ##1 state==setup ;
endproperty 
//----------------------------------------------------------------------------------------------


//----------------------------------------------------------------------------------------------
// Assertion 17a : check if state = state_closed, then these conditions should be true..
property verify_stage_seq_mult_closed;
@(posedge clk) 
  (state==st_idle) |=> mult_ready_1 == 0 &&
			   p == 0 &&
			   ctr== 0; 
endproperty 
//----------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------
// Assertion 18a : check if state = setup, then these conditions should be true..
property verify_stage_seq_mult_setup;
@(posedge clk) 
  (state==setup) |=> (enable==0 && mult_ready_1 == 0 && p == 0 && ctr == 0 && state ==st_cyc_1);
endproperty 
//----------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------
// Assertion 18b : check if state = setup, then these conditions should be true..
property verify_stage_seq_mult_setup2;
@(posedge clk) 
	(state==setup) |-> ##1 (multiplier == {{`width{a[`width-1]}}, a} && multiplicand == {{`width{b[`width-1]}}, b} );  
endproperty 
//----------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------

//----------------------------------Asserting the assertions----------------------------------------------------------------
assert property(verify_stage_seq_mult) else $error("Assertion 16a has failed");
assert property(verify_stage_seq_mult_closed) else $error("Assertion 17a has failed");
assert property(verify_stage_seq_mult_setup) else $error("Assertion 18a has failed");
assert property(verify_stage_seq_mult_setup2) else $error("Assertion 18b has failed");
//assert property(verify_stage_seq_mult_st_cyc_1) else $error("Assertion 19a has failed");
//--------------------------------------------------------------------------------------------------------------------------

  
  
	always @(posedge clk)
	begin
	if(rst) begin
	state<=st_idle;
	mult_ready_1 		= 0; //multiplier not busy
                p         = 0;
                ctr         = 0;
                prev_a=0;
          prev_b=0;
	end
	if(start&&mult_ready_1==0)
		state<=setup;
	//else if(mult_ready_1==1 && prev_a!=a && prev_b!=b)
	//	state<=setup;
	end
   always @(posedge clk)
	//if(start) begin
	case( state )
	
		 st_idle: begin
		 mult_ready_1 		= 0; //multiplier not busy
			p 		= 0;
			ctr 		= 0;
			prev_a=a;
	  prev_b=b;
		 end
		 
       setup:
		 begin
		 prev_a=a;
	  prev_b=b;
		 enable=0;
			mult_ready_1 		= 0; //multiplier busy
			p 		= 0;
			ctr 		= 0;
			multiplier 	<= {{`width{a[`width-1]}}, a}; // sign-extend
			multiplicand 	<= {{`width{b[`width-1]}}, b}; // sign-extend
			state = st_cyc_1;
			//reset=0;
     end

		st_cyc_1:
	   begin 
	 if(ctr < 26)//if (ctr < /* *** How many times should the loop run? */ ) 
	  begin
	  if(multiplier[ctr])
	  begin
	    p = p + ((2**ctr)*multiplicand);
	end
	else
	begin
	 p = p;
	 end
	 ctr = ctr + 1;
	     // *** Code for multiplication
	  end else begin
			state = st_idle;
	     mult_ready_1 = 1; 		// Assert 'rdy' signal to indicate end of multiplication
	  end
     end
	  endcase
	  //end
   
endmodule // seqmult

module dff_sync_reset(
data   , // Data Input
clk    , // Clock Input
reset  , // Reset input
q        // Q output
);
input data; 
input clk, reset ; 

output q;

reg q;

always @ ( posedge clk)
if (reset) begin
  q <= 1'b0;
end  else begin
  q <= data;
end

endmodule //End Of Module dff_sync_reset

module dff_sync_reset8 (
data   , // Data Input
clk    , // Clock Input
reset  , // Reset input
q        // Q output
);
input [7:0] data; 
input clk, reset ; 

output [7:0] q;

reg [7:0] q;

always @ ( posedge clk)
if (reset) begin
  q <= 8'b0;
end  else begin
  q <= data;
end

endmodule //End Of Module dff_sync_reset

module dff_sync_reset26 (
data   , // Data Input
clk    , // Clock Input
reset  , // Reset input
q        // Q output
);
input [25:0] data; 
input clk, reset ; 

output [25:0] q;

reg [25:0] q;

always @ ( posedge clk)
if (reset) begin
  q <= 26'b0;
end  else begin
  q <= data;
end

endmodule //End Of Module dff_sync_reset

module dff_sync_reset52 (
data   , // Data Input
clk    , // Clock Input
reset  , // Reset input
q        // Q output
);
input [51:0] data; 
input clk, reset ; 

output [51:0] q;

reg [51:0] q;

always @ ( posedge clk)
if (reset) begin
  q <= 52'b0;
end  else begin
  q <= data;
end

endmodule //End Of Module dff_sync_reset